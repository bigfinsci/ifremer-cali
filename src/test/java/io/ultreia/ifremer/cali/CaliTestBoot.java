package io.ultreia.ifremer.cali;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.jaxx.runtime.application.ApplicationBoot;
import org.nuiton.jaxx.runtime.application.ApplicationBootInitializerException;
import org.nuiton.jaxx.runtime.application.ApplicationBootInitializerSupport;

import java.util.Locale;

/**
 * Created by tchemit on 13/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CaliTestBoot extends ApplicationBootInitializerSupport<CaliConfig, CaliContext> {

    private Log log = LogFactory.getLog(RunCali.class);

    CaliTestBoot(String... args) {
        super(args);
    }

    @Override
    public CaliConfig createConfiguration(ApplicationBoot<CaliConfig, CaliContext> boot) throws ApplicationBootInitializerException {
        log.info(String.format("%s Create configuration (boot %s)", ApplicationBoot.BOOT_LOG_PREFIX, boot));
        return new CaliConfig();
    }

    @Override
    public CaliContext createContext(ApplicationBoot<CaliConfig, CaliContext> boot, CaliConfig configuration) {
        log.info(String.format("%s Create context (boot %s, configuration: %s)", ApplicationBoot.BOOT_LOG_PREFIX, boot, configuration));
        return new CaliContext(boot, configuration);
    }

    @Override
    public boolean haltOnExit() {
        return false;
    }

    @Override
    protected void initI18n(CaliConfig caliConfig) {
        I18n.close();
        I18n.init(new ClassPathI18nInitializer(), Locale.FRANCE);
    }

    @Override
    protected void initLog(CaliConfig caliConfig) {
    }

}
