package io.ultreia.ifremer.cali.api.io;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliTestRule;
import io.ultreia.ifremer.cali.api.io.command.Beep;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepOneFinalize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepOneInitialize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepTwoFinalize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepTwoInitialize;
import io.ultreia.ifremer.cali.api.io.command.CheckPresence;
import io.ultreia.ifremer.cali.api.io.command.ClearCalibrationData;
import io.ultreia.ifremer.cali.api.io.command.GetBatteryLevel;
import io.ultreia.ifremer.cali.api.io.command.GetBoardStats;
import io.ultreia.ifremer.cali.api.io.command.GetTemperatureAndHumidity;
import io.ultreia.ifremer.cali.api.io.command.SetCharacterDataEntryMode;
import io.ultreia.ifremer.cali.api.io.command.SetLengthDataEntryMode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by tchemit on 13/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BigFinDeviceTest {

    private static final Log log = LogFactory.getLog(BigFinDeviceTest.class);

    @ClassRule
    public static CaliTestRule RULE = new CaliTestRule();
    private BigFinDevice device;

    @Before
    public void setUp() throws InterruptedException {
        Thread.sleep(500);
        device = RULE.getDevice();
    }

    @After
    public void tearDown() {
        if (device != null) {
            device.close();
        }
    }

    @Ignore
    @Test
    public void checkPresence() {
        CheckPresence result = device.checkPresence();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
        Assert.assertTrue(result.isOk());
    }

    @Ignore
    @Test
    public void beep() {
        Beep result = device.beep();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
    }

    @Ignore
    @Test
    public void getBoardStats() {
        GetBoardStats result = device.getBoardStats();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
        log.info("Stats: " + result.getResponse());
    }

    @Ignore
    @Test
    public void getBatteryLevel() {
        GetBatteryLevel result = device.getBatteryLevel();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
        Assert.assertTrue(result.getBatteryLevel() > 0);
    }

    @Ignore
    @Test
    public void getTemperatureAndHumidity() {
        GetTemperatureAndHumidity result = device.getTemperatureAndHumidity();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
        Assert.assertTrue(result.getTemperature() > 0);
        Assert.assertTrue(result.getHumidity() > 0);
    }

    @Ignore
    @Test
    public void setLengthDataModeEntry() {
        SetLengthDataEntryMode result = device.setLengthDataModeEntry();
        log.info("Result: " + result);
        Assert.assertNotNull(result);
    }

    @Ignore
    @Test
    public void setCharacterDataModeEntry() {
        SetCharacterDataEntryMode result = device.setCharacterDataModeEntry();
        log.info("Result: " + result);
        Assert.assertNotNull(result);
    }

    @Ignore
    @Test
    public void clearCalibrationData() {
        ClearCalibrationData result = device.clearCalibrationData();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
    }

    @Ignore
    @Test
    public void calibration() {
        clearCalibrationData();
        calibrationStepOneInitialize();
        calibrationStepOneFinalize();
        calibrationStepTwoInitialize();
        calibrationStepTwoFinalize();
    }

    private void calibrationStepOneInitialize() {
        CalibrationStepOneInitialize result = device.calibrationStepOneInitialize(0);
        Assert.assertNotNull(result);
        log.info("Result: " + result);
    }

    private void calibrationStepOneFinalize() {
        CalibrationStepOneFinalize result = device.calibrationStepOneFinalize();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
        Assert.assertNotNull(result.getRawValue());
    }

    private void calibrationStepTwoInitialize() {
        CalibrationStepTwoInitialize result = device.calibrationStepTwoInitialize(375);
        Assert.assertNotNull(result);
        log.info("Result: " + result);
    }

    private void calibrationStepTwoFinalize() {
        CalibrationStepTwoFinalize result = device.calibrationStepTwoFinalize();
        Assert.assertNotNull(result);
        log.info("Result: " + result);
        Assert.assertNotNull(result.getRawValue());
    }

}
