package io.ultreia.ifremer.cali.boot;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliConfig;
import io.ultreia.ifremer.cali.CaliContext;
import io.ultreia.ifremer.cali.ui.main.MainUI;
import org.nuiton.jaxx.runtime.application.command.WithMainUICommandSupport;

import java.awt.Frame;

/**
 * Created by tchemit on 09/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class RunMainUI extends WithMainUICommandSupport<CaliConfig, CaliContext> {
    @Override
    protected void run(Frame ui) {
        MainUI mainUI = (MainUI) ui;
        if (mainUI.getConfig().isGetBoardsOnStart()) {
            mainUI.getGetBoards().doClick();
            try {
                mainUI.getContext().lock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (mainUI.getConfig().isSelectFirstBoardOnStart() && !mainUI.getModel().isDetectedBoardsEmpty()) {
                mainUI.getBoards().setSelectedIndex(0);
            }
        }
    }
}
