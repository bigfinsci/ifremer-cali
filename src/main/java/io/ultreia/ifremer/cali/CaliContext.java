package io.ultreia.ifremer.cali;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import io.ultreia.ifremer.cali.api.BoardApi;
import io.ultreia.ifremer.cali.api.io.BigFinCommandEngine;
import io.ultreia.ifremer.cali.api.io.BigFinDevicesManager;
import io.ultreia.ifremer.cali.ui.main.MainUI;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.application.ApplicationBoot;
import org.nuiton.jaxx.runtime.application.ApplicationContextComponent;
import org.nuiton.jaxx.runtime.application.ApplicationInstances;
import org.nuiton.jaxx.runtime.application.DefaultApplicationContext;
import org.nuiton.jaxx.runtime.application.UserMessages;
import org.nuiton.jaxx.runtime.swing.session.SwingSessionHelper;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;

import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;

/**
 * Created by tchemit on 08/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CaliContext extends DefaultApplicationContext<CaliConfig, CaliContext> {

    private static final CaliApplicationVariable<MainUI> MainUI = new CaliApplicationVariable<>(MainUI.class, "Cali Main UI");
    private static final CaliComponent<BigFinCommandEngine> BIG_FIN_COMMAND_ENGINE = new CaliComponent<BigFinCommandEngine>(BigFinCommandEngine.class) {
        @Override
        public BigFinCommandEngine load(CaliContext context, CaliConfig config) {
            return new BigFinCommandEngine(context.getBoot().getThreadPoolExecutor());
        }
    };
    private static final CaliComponent<BigFinDevicesManager> BIG_FIN_DEVICES = new CaliComponent<BigFinDevicesManager>(BigFinDevicesManager.class) {
        @Override
        public BigFinDevicesManager load(CaliContext context, CaliConfig config) {
            return new BigFinDevicesManager(3, context.getBoot().getThreadPoolExecutor(), context.getBigFinCommandEngine());
        }
    };
    private static final CaliComponent<BoardApi> BOARD_API = new CaliComponent<BoardApi>(BoardApi.class) {
        @Override
        public BoardApi load(CaliContext context, CaliConfig config) {
            return new BoardApi(context.getBigFinDevices());
        }
    };
    private static final Log log = LogFactory.getLog(CaliContext.class);
    private static final ApplicationContextComponent<CaliConfig, CaliContext, SwingSessionHelper> SWING_SESSION_HELPER = SWING_SESSION_HELPER();

    public CaliContext(ApplicationBoot<CaliConfig, CaliContext> boot, CaliConfig config) {
        super(boot, config, ImmutableList.of(SWING_SESSION_HELPER, BIG_FIN_COMMAND_ENGINE, BIG_FIN_DEVICES, BOARD_API), ImmutableList.of(MainUI));
    }

    public static CaliContext context() {
        return ApplicationInstances.context(CaliContext.class);
    }

    public BigFinDevicesManager getBigFinDevices() {
        return BIG_FIN_DEVICES.get();
    }

    public BigFinCommandEngine getBigFinCommandEngine() {
        return BIG_FIN_COMMAND_ENGINE.get();
    }

    public BoardApi getBoardApi() {
        return BOARD_API.get();
    }

    @Override
    public MainUI getMainUI() {
        return MainUI.get();
    }

    public void setMainUI(MainUI ui) {
        MainUI.set(ui);
    }

    @Override
    public MainUI newMainUI() {
        MainUI ui = new MainUI(toInitialContext());
        boolean fullScreen = getConfig().isFullScreen();
        ui.setUndecorated(fullScreen);
        ErrorDialogUI.init(ui);
        UserMessages.setMainUI(ui);
        // set full screen property on main ui
        ui.getGraphicsConfiguration().getDevice().setFullScreenWindow(fullScreen ? ui : null);
        setMainUI(ui);
        return ui;
    }

    @Override
    public final void removeMainUI() {
        MainUI mainUI = MainUI.remove();
        mainUI.dispose();
        mainUI.setVisible(false);

        getConfig().removeJaxxPropertyChangeListener();

        ApplicationBoot.cleanMemory();
    }

    @Override
    public void setMainUIVisible(Frame ui, boolean replace) {
        SwingUtilities.invokeLater(() -> {
            ui.setVisible(true);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            ui.setLocation(dim.width / 2 - ui.getSize().width / 2, dim.height / 2 - ui.getSize().height / 2);
        });
        getSwingSessionHelper().addComponent(ui, replace);
    }

    @Override
    public void displayInfo(String startMessage) {
        if (useMainUI()) {
            getMainUI().displayInfo(startMessage);
        } else {
            log.info(startMessage);
        }
    }

    @Override
    public void notBusy() {
        getConfig().setBusy(false);
    }

    @Override
    public void busy() {
        getConfig().setBusy(true);
    }

    public boolean useMainUI() {
        return getMainUI() != null;
    }

    @Override
    public void handlingError(Exception e) {
        handlingError(e.getMessage(), e);
    }

    @Override
    public void handlingError(String message, Exception e) {
        log.error(message, e);
        ErrorDialogUI.showError(e);
    }

    public void reloadApplication() {
        getBoot().restart();
    }

    @Override
    public SwingSessionHelper getSwingSessionHelper() {
        return SWING_SESSION_HELPER.get();
    }

    static class CaliApplicationVariable<O> extends ApplicationContextComponent<CaliConfig, CaliContext, O> {
        private CaliApplicationVariable(Class<O> type, String name) {
            super(type, name);
        }

        @Override
        public final String toString() {
            return String.format("Cali.variable{%s::%s}", getName(), getType().getSimpleName());
        }

        @Override
        protected final O load(CaliContext context, CaliConfig config) {
            throw new NotImplementedException("Can't load a variable.");
        }
    }

    abstract static class CaliComponent<O> extends ApplicationContextComponent<CaliConfig, CaliContext, O> {
        private CaliComponent(Class<O> type) {
            super(type);
        }

        @Override
        public final String toString() {
            return String.format("Cali.component{%s::%s}", getName(), getType().getSimpleName());
        }
    }


}
