package io.ultreia.ifremer.cali;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfigInit;
import org.nuiton.config.ApplicationConfigScope;
import org.nuiton.jaxx.runtime.application.ApplicationConfiguration;
import org.nuiton.jaxx.runtime.application.ApplicationInstances;
import org.nuiton.jaxx.runtime.resources.UIResourcesProvider;

import java.beans.PropertyChangeListener;

public class CaliConfig extends GeneratedCaliConfig implements ApplicationConfiguration {

    public static final String PROPERTY_BUSY = "busy";
    private static final Log log = LogFactory.getLog(CaliConfig.class);
    private boolean busy;

    public CaliConfig() {
        this(ApplicationConfigInit.forAllScopesWithout(ApplicationConfigScope.CURRENT));
    }

    public CaliConfig(ApplicationConfigInit init) {
        super(init.setConfigFileName("cali.conf"));
    }

    public static CaliConfig configuration() {
        return ApplicationInstances.configuration(CaliConfig.class);
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
        firePropertyChange("busy", busy);
    }

    public void removeJaxxPropertyChangeListener() {
//        pcs.getPropertyChangeListeners();
//        List<String> tmp = new ArrayList<>();
//        for (ClientConfigOption option : ClientConfigOption.values()) {
//            String propertyName = option.getPropertyKey();
//            if (propertyName != null) {
//                tmp.add(propertyName);
//            }
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("property names to seek for options : " + tmp);
//        }
//        String[] propertyNames = tmp.toArray(
//                new String[tmp.size()]);
//
//        PropertyChangeListener[] toRemove =
//                SwingUtil.findJaxxPropertyChangeListener(
//                        propertyNames,
//                        getPropertyChangeListeners());
//        if (toRemove == null || toRemove.length == 0) {
//            return;
//        }
        log.debug("before remove : " + pcs.getPropertyChangeListeners().length);
        for (PropertyChangeListener listener : pcs.getPropertyChangeListeners()) {
            removePropertyChangeListener(listener);
        }
        log.debug("after remove : " + pcs.getPropertyChangeListeners().length);
    }

    @AutoService(UIResourcesProvider.class)
    public static class CaliUIResourcesProvider extends UIResourcesProvider {
        public CaliUIResourcesProvider() {
            super("Cali application", "META-INF/ui/cali-ui.properties");
        }
    }
}
