package io.ultreia.ifremer.cali.api.io;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * To send command to the ichtyometer and read his reponses.
 * <p>
 * Created on 1/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BigFinCommandEngine {

    private static final Log log = LogFactory.getLog(BigFinCommandEngine.class);
    /**
     * To execute command.
     */
    private final ThreadPoolExecutor service;
    /**
     * Last submit command.
     */
    private Future<?> submit;

    public BigFinCommandEngine(ThreadPoolExecutor service) {
        this.service = Objects.requireNonNull(service);
    }

    public <R extends BigFinCommand> R sendCommand(BigFinDevice device, R request) {
        try {
            Objects.requireNonNull(device, "device can not be null");
        Objects.requireNonNull(request, "request can not be null");
            log.info(String.format("Send command: %s (type: %s) from %s", request.getQuestion(), request.getClass().getSimpleName(), device.getName()));
            (submit = service.submit(() -> run(device, request))).get(1, TimeUnit.MINUTES);
            return request;
        } catch (InterruptedException | TimeoutException e) {
            throw new BigFinCommandExecutionException(request, device, String.format("Time out on command %s", request.getQuestion()), e);
        } catch (CancellationException e) {
            throw new BigFinCommandExecutionException(request, device, String.format("Execution cancel on command %s", request.getQuestion()), e.getCause());
        } catch (ExecutionException e) {
            throw new BigFinCommandExecutionException(request, device, String.format("Execution error on command %s", request.getQuestion()), e.getCause());
        }
    }

    public void stopLastCommand() {
        if (submit != null && !submit.isDone()) {
            submit.cancel(true);
        }
    }

    private void run(BigFinDevice device, BigFinCommand request) {

        try {
            // send question
            String command = request.getQuestion();
            if (command != null) {
                device.writeChars(command);
            }
            StringBuilder responseBuilder = new StringBuilder();
            if (request.isNeedResponse()) {
                String response;
                boolean responseComplete = false;
                while (!responseComplete) {
                    String message = device.readMessage();
                    responseBuilder.append(message);
                    responseComplete = request.isResponseComplete(message);
                }
                response = responseBuilder.toString();
                request.setResponse(response);
            }
        } catch (Exception e) {
            log.error(e);
        } finally {
            submit = null;
        }
    }

}
