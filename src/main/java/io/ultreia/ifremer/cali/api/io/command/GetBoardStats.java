package io.ultreia.ifremer.cali.api.io.command;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.io.BigFinCommand;

/**
 * Created by tchemit on 12/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GetBoardStats extends BigFinCommand {

    private static final String REQUEST = "b#";
    private static final String RESPONSE_PREFIX = "%b:";

    private String boardType;
    private String firmwareVersion;
    public GetBoardStats() {
        super(REQUEST, true);
    }

    @Override
    public boolean isResponseComplete(String response) {
        return response != null && response.trim().startsWith(RESPONSE_PREFIX);
    }

    @Override
    public void setResponse(String response) {
        super.setResponse(response);
        if (response == null) {
            boardType = firmwareVersion = null;
        } else {
            String[] parts = response.substring(RESPONSE_PREFIX.length()).split(",");
            if (parts.length < 2) {
                throw new IllegalStateException(String.format("Should get at last two values on return message:%s", response));
            }
            boardType = parts[0].trim();
            firmwareVersion = parts[1].trim();
        }
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public String getBoardType() {
        return boardType;
    }
}
