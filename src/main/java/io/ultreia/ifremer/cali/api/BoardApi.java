package io.ultreia.ifremer.cali.api;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliConfig;
import io.ultreia.ifremer.cali.api.io.BigFinDevice;
import io.ultreia.ifremer.cali.api.io.BigFinDevicesManager;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepOneFinalize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepOneInitialize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepTwoFinalize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepTwoInitialize;
import io.ultreia.ifremer.cali.api.io.command.ClearCalibrationData;
import io.ultreia.ifremer.cali.api.io.command.GetBatteryLevel;
import io.ultreia.ifremer.cali.api.io.command.GetBoardStats;
import io.ultreia.ifremer.cali.api.io.command.GetTemperatureAndHumidity;
import io.ultreia.ifremer.cali.api.io.command.SetLengthDataEntryMode;
import io.ultreia.ifremer.cali.api.io.record.BigFinDeviceRecordListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BoardApi {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EEEE, d MMM yyyy HH:mm:ss");

    private static final Log log = LogFactory.getLog(BoardApi.class);

    private final BigFinDevicesManager devicesManager;

    public BoardApi(BigFinDevicesManager devicesManager) {
        this.devicesManager = devicesManager;
    }

    public List<String> getBoardNames() {
        return devicesManager.listDevices(true);
    }

    public Board connect(String boardName) {

        BigFinDevice device = devicesManager.openDevice(boardName);
        log.info(String.format("New open device: %s", device));
        Board board = new Board();
        board.setDevice(device);
        board.setBoardName(device.getName());

        try {
            GetBoardStats boardStats = device.getBoardStats();
            board.setFirmwareVersion(boardStats.getFirmwareVersion());
            board.setBoardType(boardStats.getBoardType());
        } catch (Exception e) {
            board.setDevice(null);
            throw e;
        }
        try {
            updateValues(board);
        } catch (Exception e) {
            board.setDevice(null);
            throw e;
        }
        try {
            setLengthDataEntryMode(board);
        } catch (Exception e) {
            board.setDevice(null);
            throw e;
        }

        return board;
    }

    public void disconnect(Board board) {
        devicesManager.closeOpenDevice();
        board.setDevice(null);
    }

    public void updateValues(Board board) {
        BigFinDevice device = board.getDevice();

        log.info("Board firmware: " + board.getFirmwareVersion());
        GetBatteryLevel batteryLevel = device.getBatteryLevel();
        board.setBatteryLevel(batteryLevel.getBatteryLevel());

        boolean askTemperatureAndHumidity = board.isCanAskFormTemperatureAndHumidity();
        if (askTemperatureAndHumidity) {
            GetTemperatureAndHumidity temperatureAndHumidity = device.getTemperatureAndHumidity();
            board.setTemperature(temperatureAndHumidity.getTemperature());
            board.setHumidity(temperatureAndHumidity.getHumidity());
        }
        //FIXME
//        board.setDataEntryMode(BoardDataEntryMode.CHARACTER);
        board.setLastUpdate(new Date());
    }

    public Calibration startCalibration(Board board) {
        Calibration calibration = new Calibration(board);
        calibration.setStepOneRequest(CaliConfig.configuration().getCalibrationDefaultStepOne());
        calibration.setStepTwoRequest(CaliConfig.configuration().getCalibrationDefaultStepTwo());
        return calibration;
    }

    public void processCalibration(Calibration calibration) {
        BigFinDevice device = calibration.getBoard().getDevice();

        // init
        ClearCalibrationData clearCalibrationData = device.clearCalibrationData();
        log.info(String.format("clear calibration data command result: %s", clearCalibrationData));
        int stepOneValue = Objects.requireNonNull(calibration.getStepOneRequest());
        CalibrationStepOneInitialize calibrationStepOneInitialize = device.calibrationStepOneInitialize(stepOneValue);
        log.info(String.format("step one initialize command result: %s", calibrationStepOneInitialize));
        int stepTwoValue = Objects.requireNonNull(calibration.getStepTwoRequest());
        CalibrationStepTwoInitialize calibrationStepTwoInitialize = device.calibrationStepTwoInitialize(stepTwoValue);
        log.info(String.format("step two initialize command result: %s", calibrationStepTwoInitialize));
        calibration.setInitialize(true);
        // get raw value 1
        CalibrationStepOneFinalize calibrationStepOneFinalize = device.calibrationStepOneFinalize();
        log.info(String.format("step one finalize command result: %s", calibrationStepOneFinalize));
        calibration.setStepOneResult(calibrationStepOneFinalize.getRawValue());
        // get raw value 2
        CalibrationStepTwoFinalize calibrationStepTwoFinalize = device.calibrationStepTwoFinalize();
        log.info(String.format("step two finalize command result: %s", calibrationStepTwoFinalize));
        calibration.setStepTwoResult(calibrationStepTwoFinalize.getRawValue());

    }

    public Measure startMeasureMode(Board board, BigFinDeviceRecordListener listener) {
        Measure measure = new Measure(board);
        board.getDevice().startMeasureMode(listener);
        return measure;
    }

    public void stopMeasureMode(Board board) {
        board.getDevice().stopMeasureMode();
    }

    private void setLengthDataEntryMode(Board board) {
        SetLengthDataEntryMode result = board.getDevice().setLengthDataModeEntry();
        log.info(String.format("command result: %s", result));
        board.setDataEntryMode(BoardDataEntryMode.LENGTH);
    }

}
