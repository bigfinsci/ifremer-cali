package io.ultreia.ifremer.cali.api.io.command;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.io.BigFinCommand;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 12/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GetBatteryLevel extends BigFinCommand {

    private static final Log log = LogFactory.getLog(GetBatteryLevel.class);
    private static final String REQUEST = "&q#\r";
    private static final String RESPONSE = "%q:";
    private Integer batteryLevel;

    public GetBatteryLevel() {
        super(REQUEST, true);
    }

    @Override
    public boolean isResponseComplete(String response) {
        return response != null && response.startsWith(RESPONSE);
    }

    @Override
    public void setResponse(String response) {
        super.setResponse(response);
        if (response != null) {
            try {
                String trim = StringUtils.removeStart(response, RESPONSE).trim();
                trim = trim.substring(0, trim.indexOf(','));
                this.batteryLevel = Integer.valueOf(trim);
            } catch (Exception e) {
                log.error("Can't get battery level from response: " + response, e);
                batteryLevel = null;
            }
        }
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }
}
