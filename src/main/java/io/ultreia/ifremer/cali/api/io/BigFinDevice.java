package io.ultreia.ifremer.cali.api.io;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliContext;
import io.ultreia.ifremer.cali.api.io.command.Beep;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepOneFinalize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepOneInitialize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepTwoFinalize;
import io.ultreia.ifremer.cali.api.io.command.CalibrationStepTwoInitialize;
import io.ultreia.ifremer.cali.api.io.command.CheckPresence;
import io.ultreia.ifremer.cali.api.io.command.ClearCalibrationData;
import io.ultreia.ifremer.cali.api.io.command.GetBatteryLevel;
import io.ultreia.ifremer.cali.api.io.command.GetBoardStats;
import io.ultreia.ifremer.cali.api.io.command.GetTemperatureAndHumidity;
import io.ultreia.ifremer.cali.api.io.command.SetCharacterDataEntryMode;
import io.ultreia.ifremer.cali.api.io.command.SetLengthDataEntryMode;
import io.ultreia.ifremer.cali.api.io.message.BigFinDeviceMessageEvent;
import io.ultreia.ifremer.cali.api.io.message.BigFinDeviceMessageListener;
import io.ultreia.ifremer.cali.api.io.message.BigFinMessageConsumer;
import io.ultreia.ifremer.cali.api.io.record.BigFinDeviceRecordConsumer;
import io.ultreia.ifremer.cali.api.io.record.BigFinDeviceRecordListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.swing.event.EventListenerList;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Represents a remote device.
 * <p>
 * Created by tchemit on 12/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BigFinDevice implements Closeable {

    private static final Log log = LogFactory.getLog(BigFinDevice.class);
    /**
     * Command engine.
     */
    private final BigFinCommandEngine commandEngine;
    /**
     * Friendly name of the device.
     */
    private final String name;
    /**
     * Url connection to the remote device.
     */
    private final String connectionUrl;
    /**
     * To keep list of {@link BigFinDeviceMessageListener} listeners.
     */
    private final EventListenerList listenerList;
    /**
     * Message consumer.
     */
    private final BigFinMessageConsumer messageConsumer;
    /**
     * Unique opened connection.
     */
    private StreamConnection connection;
    /**
     * Unique opened data input stream.
     */
    private DataInputStream dataInputStream;
    /**
     * Unique opened data output stream.
     */
    private DataOutputStream dataOutputStream;
    /**
     * Internal close state.
     */
    private boolean closed;
    /**
     * Feed reader.
     */
    private BigFinDeviceRecordConsumer feedReader;

    BigFinDevice(ThreadPoolExecutor threadPoolExecutor, BigFinCommandEngine commandEngine, String name, String connectionUrl) {
        this.commandEngine = Objects.requireNonNull(commandEngine);
        this.connectionUrl = Objects.requireNonNull(connectionUrl);
        this.name = Objects.requireNonNull(name);
        this.listenerList = new EventListenerList();
        this.messageConsumer = new BigFinMessageConsumer(this);
        this.messageConsumer.start(Objects.requireNonNull(threadPoolExecutor));
    }

    public String getName() {
        return name;
    }

    public void addBigFinDeviceMessageListener(BigFinDeviceMessageListener listener) {
        listenerList.add(BigFinDeviceMessageListener.class, listener);
    }

    public void removeBigFinDeviceMessageListener(BigFinDeviceMessageListener listener) {
        listenerList.remove(BigFinDeviceMessageListener.class, listener);
    }

    public void removeBigFinDeviceMessageListeners() {
        for (BigFinDeviceMessageListener listener : listenerList.getListeners(BigFinDeviceMessageListener.class)) {
            listenerList.remove(BigFinDeviceMessageListener.class, listener);
        }
    }

    public CheckPresence checkPresence() {
        return sendCommand(new CheckPresence());
    }

    public Beep beep() {
        return sendCommand(new Beep());
    }

    public GetBatteryLevel getBatteryLevel() {
        return sendCommand(new GetBatteryLevel());
    }

    public GetTemperatureAndHumidity getTemperatureAndHumidity() {
        return sendCommand(new GetTemperatureAndHumidity());
    }

    public SetLengthDataEntryMode setLengthDataModeEntry() {
        return sendCommand(new SetLengthDataEntryMode());
    }

    public SetCharacterDataEntryMode setCharacterDataModeEntry() {
        return sendCommand(new SetCharacterDataEntryMode());
    }

    public ClearCalibrationData clearCalibrationData() {
        return sendCommand(new ClearCalibrationData());
    }

    public GetBoardStats getBoardStats() {
        return sendCommand(new GetBoardStats());
    }

    public CalibrationStepOneInitialize calibrationStepOneInitialize(int calibrationValue) {
        return sendCommand(new CalibrationStepOneInitialize(calibrationValue));
    }

    public CalibrationStepOneFinalize calibrationStepOneFinalize() {
        return sendCommand(new CalibrationStepOneFinalize());
    }

    public CalibrationStepTwoInitialize calibrationStepTwoInitialize(int calibrationValue) {
        return sendCommand(new CalibrationStepTwoInitialize(calibrationValue));
    }

    public CalibrationStepTwoFinalize calibrationStepTwoFinalize() {
        return sendCommand(new CalibrationStepTwoFinalize());
    }

    public boolean isClosed() {
        return closed;
    }

    public boolean isOpen() {
        return !closed;
    }

    @Override
    public void close() {
        if (closed) {
            return;
        }
        closed = true;

        stopMeasureMode();

        try {
            messageConsumer.close();
        } catch (Exception e) {
            log.error("Could not close message consumer", e);
        }
        if (dataInputStream != null) {
            try {
                dataInputStream.close();
            } catch (Exception e) {
                log.error("Could not close data input stream", e);
            }
        }
        if (dataOutputStream != null) {
            try {
                dataOutputStream.close();
            } catch (Exception e) {
                log.error("Could not close data output stream", e);
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception e) {
                log.error(String.format("Could not close connection: %s", connection), e);
            } finally {
                connection = null;
            }
        }
    }

    public String readMessage() throws InterruptedException {
        if (isClosed()) {
            return null;
        }
        String message = messageConsumer.readMessage();
        if (message != null) {
            log.info(String.format("IO - Reading %s", message));
        }
        return message;
    }

    public <R extends BigFinCommand> R sendCommand(R request) {
        return commandEngine.sendCommand(this, request);
    }

    public void startMeasureMode(BigFinDeviceRecordListener listener) {
        checkIsOpen();
        this.feedReader = new BigFinDeviceRecordConsumer(this);
        this.feedReader.addFeedModeReaderListener(listener);
        CaliContext context = CaliContext.context();
        try {
            this.feedReader.start(context.getBoot().getThreadPoolExecutor());
        } catch (Exception e) {
            context.handlingError("Impossible de démarrer le mode mesure", e);
            stopMeasureMode();
            throw e;
        }
    }

    public void stopMeasureMode() {
        if (feedReader != null) {
            try {
                try {
                    feedReader.close();
                } catch (Exception e) {
                    log.error("Could not close feed reader", e);
                }
            } finally {
                feedReader = null;
            }
        }
    }

    public synchronized void writeChars(String command) throws IOException {
        checkIsOpen();
        byte[] bytes = (command).getBytes(StandardCharsets.US_ASCII);
        log.info(String.format("IO - Sending %s", command));
        log.info(String.format("IO - Sending %s", Arrays.toString(bytes)));
        getDataOutputStream().write(bytes);

    }

    public synchronized DataInputStream getDataInputStream() throws IOException {
        return dataInputStream == null ? dataInputStream = getConnection().openDataInputStream() : dataInputStream;
    }

    public void fireMessageReceived(String message) {
        BigFinDeviceMessageEvent event = null;
        for (BigFinDeviceMessageListener listener : listenerList.getListeners(BigFinDeviceMessageListener.class)) {
            if (event == null) {
                event = new BigFinDeviceMessageEvent(this, message);
            }
            listener.messageReceived(event);
        }
    }

    private synchronized DataOutputStream getDataOutputStream() throws IOException {
        return dataOutputStream == null ? dataOutputStream = getConnection().openDataOutputStream() : dataOutputStream;
    }

    private synchronized StreamConnection getConnection() throws IOException {
        checkIsOpen();
        return connection == null ? connection = (StreamConnection) Connector.open(connectionUrl, Connector.READ_WRITE) : connection;
    }

    private void checkIsOpen() {
        if (closed) {
            throw new IllegalStateException(String.format("BigFin device %s is not open!", name));
        }
    }
}
