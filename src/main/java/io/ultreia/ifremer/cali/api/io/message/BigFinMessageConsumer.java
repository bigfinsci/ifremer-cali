package io.ultreia.ifremer.cali.api.io.message;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import io.ultreia.ifremer.cali.api.io.BigFinDevice;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by tchemit on 14/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BigFinMessageConsumer implements Runnable, Closeable {

    private static final Log log = LogFactory.getLog(BigFinMessageConsumer.class);
    /**
     * BigFin device.
     */
    private final BigFinDevice device;
    /**
     * Lock to wait for next message.
     */
    private final Object lock;
    /**
     * Flag to stop the runnable.
     */
    private boolean stop;
    /**
     * Last message received.
     */
    private String lastMessage;
    /**
     * Log prefix.
     */
    private final String prefix;
    /**
     * Internal thread to wait for next message.
     * <p>
     * <b>Note:</b> we can't use here simple runner in thread pool executor, since the thread must be stop here asap.
     */
    private Thread thread;

    public BigFinMessageConsumer(BigFinDevice device) {
        this.device = Objects.requireNonNull(device);
        this.prefix = String.format("Messages consumer [device: %s]", device.getName());
        this.lock = new Object();
    }

    public void start(ThreadPoolExecutor threadPoolExecutor) {
        Preconditions.checkState(device.isOpen(), "device must be opened");
        thread = Objects.requireNonNull(threadPoolExecutor).getThreadFactory().newThread(this);
        thread.start();
        log.info(String.format("%s Ready to read messages.", prefix));
    }

    @Override
    public void close() {
        stop = true;
        try {
            thread.join(500);
        } catch (InterruptedException e) {
            log.error(String.format("%s Could not close thread", prefix), e);
        } finally {
            thread = null;
        }
    }

    @Override
    public void run() {
        log.info(String.format("%s start %s", prefix, this));
        DataInputStream dataInputStream = null;
        while (!stop) {
            try {
                if (dataInputStream == null) {
                    dataInputStream = device.getDataInputStream();
                }
                lastMessage = readMessage(dataInputStream);
                log.info(String.format("%s new message received: %s", prefix, lastMessage));
                log.info(String.format("IO - Reading %s", lastMessage));
                if (!lastMessage.isEmpty()) {
                    device.fireMessageReceived(lastMessage);
                }
                unlock();
            } catch (Exception e) {
                log.error(String.format("%s Could not read message", prefix), e);
            }
        }
        log.info(String.format("%s stop %s", prefix, this));
    }

    public String readMessage() throws InterruptedException {
        lock();
        return lastMessage;
    }

    private String readMessage(DataInputStream dataInputStream) throws IOException {
        if (stop) {
            return "";
        }
        log.debug(String.format("%s Waiting to read new message", prefix));
        StringBuilder result = new StringBuilder();
        // wait until got a #
        boolean complete = false;
        while (!complete) {
            if (stop) {
                break;
            }
            while (dataInputStream.available() > 0) {
                if (complete || stop) {
                    break;
                }
                int c = dataInputStream.read();
                result.append((char) c);
                if ('#' == (char) c) {
                    complete = true;
                }
            }
        }
        String message = result.toString().trim();
        log.debug(String.format("%s New message: %s", prefix, message));
        return message;
    }

    private void lock() throws InterruptedException {
        synchronized (lock) {
            lock.wait();
        }
    }

    private void unlock() {
        synchronized (lock) {
            lock.notifyAll();
        }
    }

}
