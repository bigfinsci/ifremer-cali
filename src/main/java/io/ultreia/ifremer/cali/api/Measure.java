package io.ultreia.ifremer.cali.api;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.io.record.BigFinDeviceMeasureRecord;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tchemit on 14/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class Measure extends AbstractJavaBean {

    private final Board board;

    private final List<BigFinDeviceMeasureRecord> measures;
    private BigFinDeviceMeasureRecord lastMeasure;

    public Measure(Board board) {
        this.board = board;
        this.measures = new LinkedList<>();
    }

    public String getBoardName() {
        return board.getBoardName();
    }

    public Board getBoard() {
        return board;
    }

    public List<BigFinDeviceMeasureRecord> getMeasures() {
        return measures;
    }

    public BigFinDeviceMeasureRecord getLastMeasure() {
        return lastMeasure;
    }

    public void setLastMeasure(BigFinDeviceMeasureRecord lastMeasure) {
        BigFinDeviceMeasureRecord oldValue = getLastMeasure();
        this.lastMeasure = lastMeasure;
        firePropertyChange("lastMeasure", oldValue, lastMeasure);
    }

    public int getMeasuresCount() {
        return measures.size();
    }

}
