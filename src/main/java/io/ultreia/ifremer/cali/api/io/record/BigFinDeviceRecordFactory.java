package io.ultreia.ifremer.cali.api.io.record;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 12/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class BigFinDeviceRecordFactory {

    private final Set<FeedReaderRecordAcceptor> acceptors;

    BigFinDeviceRecordFactory() {
        acceptors = new LinkedHashSet<>();
        acceptors.add(new FeedReaderRecordAcceptor<BigFinDeviceStylusMotionRecord>() {
            @Override
            public boolean accept(String record) {
                return BigFinDeviceStylusMotionRecord.acceptRecord(record);
            }

            @Override
            public BigFinDeviceStylusMotionRecord newRecord(String record, Date date) {
                return new BigFinDeviceStylusMotionRecord(record, date);
            }
        });
        acceptors.add(new FeedReaderRecordAcceptor<BigFinDeviceMeasureRecord>() {
            @Override
            public boolean accept(String record) {
                return BigFinDeviceMeasureRecord.acceptRecord(record);
            }

            @Override
            public BigFinDeviceMeasureRecord newRecord(String record, Date date) {
                return new BigFinDeviceMeasureRecord(record, date);
            }
        });

    }

    public BigFinDeviceRecordSupport newRecord(String record, Date date) {

        BigFinDeviceRecordSupport feedRecord = null;

        for (FeedReaderRecordAcceptor acceptor : acceptors) {

            boolean accept = acceptor.accept(record);
            if (accept) {

                feedRecord = acceptor.newRecord(record, date);
                break;

            }

        }

        return feedRecord;

    }

    protected interface FeedReaderRecordAcceptor<F extends BigFinDeviceRecordSupport> {

        boolean accept(String record);


        F newRecord(String record, Date date);

    }

}
