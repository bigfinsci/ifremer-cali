package io.ultreia.ifremer.cali.api;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.io.BigFinDevice;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Date;
import java.util.Objects;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class Board extends AbstractJavaBean {

    private BigFinDevice device;
    private String boardName;
    private Integer batteryLevel;
    private Integer temperature;
    private Integer humidity;
    private BoardDataEntryMode dataEntryMode;
    private Date lastUpdate;
    private String boardType;
    private String firmwareVersion;

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        String oldValue = getBoardName();
        this.boardName = boardName;
        firePropertyChange("boardName", oldValue, boardName);
    }

    public boolean isConnected() {
        return device != null;
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Integer batteryLevel) {
        Integer oldValue = getBatteryLevel();
        this.batteryLevel = batteryLevel;
        firePropertyChange("batteryLevel", oldValue, batteryLevel);
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        Integer oldValue = getTemperature();
        this.temperature = temperature;
        firePropertyChange("temperature", oldValue, temperature);
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        Integer oldValue = getHumidity();
        this.humidity = humidity;
        firePropertyChange("humidity", oldValue, humidity);
    }

    public BoardDataEntryMode getDataEntryMode() {
        return dataEntryMode;
    }

    public void setDataEntryMode(BoardDataEntryMode dataEntryMode) {
        BoardDataEntryMode oldValue = getDataEntryMode();
        this.dataEntryMode = dataEntryMode;
        firePropertyChange("dataEntryMode", oldValue, dataEntryMode);
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        Date oldValue = getLastUpdate();
        this.lastUpdate = lastUpdate;
        firePropertyChange("lastUpdate", oldValue, lastUpdate);
    }

    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
        firePropertyChange("boardType", boardType);
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
        firePropertyChange("firmwareVersion", firmwareVersion);
    }

    public BigFinDevice getDevice() {
        return device;
    }

    public void setDevice(BigFinDevice device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Board that = (Board) o;
        return Objects.equals(boardName, that.boardName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(boardName);
    }

    public boolean isCanAskFormTemperatureAndHumidity() {
        return firmwareVersion != null && !"225".equals(firmwareVersion);
    }
}
