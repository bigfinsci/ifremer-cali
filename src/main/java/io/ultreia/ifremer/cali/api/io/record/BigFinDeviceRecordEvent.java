package io.ultreia.ifremer.cali.api.io.record;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.EventObject;

/**
 * Created on 1/24/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class BigFinDeviceRecordEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    protected final BigFinDeviceMeasureRecord record;

    public BigFinDeviceRecordEvent(BigFinDeviceRecordConsumer source, BigFinDeviceMeasureRecord record) {
        super(source);
        this.record = record;
    }

    @Override
    public BigFinDeviceRecordConsumer getSource() {
        return (BigFinDeviceRecordConsumer) super.getSource();
    }

    public BigFinDeviceMeasureRecord getRecord() {
        return record;
    }
}
