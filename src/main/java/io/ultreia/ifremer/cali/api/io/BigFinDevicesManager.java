package io.ultreia.ifremer.cali.api.io;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.intel.bluetooth.BlueCoveImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DataElement;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * To manage all big fin devices.
 * <p>
 * Created by tchemit on 12/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BigFinDevicesManager implements Closeable {

    private static final Log log = LogFactory.getLog(BigFinDevicesManager.class);
    /**
     * Maximum try to connect.
     */
    private final int maximumNumberOfTryToConnect;
    /**
     * Thread pool executor (to create new threads).
     */
    private final ThreadPoolExecutor threadPoolExecutor;
    /**
     * Command engine.
     */
    private final BigFinCommandEngine commandEngine;
    /**
     * Local bluetooth device (lazy loaded).
     */
    private LocalDevice localDevice;
    /**
     * To keep a cache of discovered remote device. Keys are the name of device, values are the remote device  (lazy loaded).
     */
    private Map<String, RemoteDevice> remoteDevices;
    /**
     * To keep a cache of discovered connection url on remote devices. Keys are the name of device, values are
     * the connection of the remote device  (lazy loaded).
     */
    private Map<String, String> remoteConnectionUrlCache;
    /**
     * Unique open device.
     */
    private BigFinDevice openDevice;

    public BigFinDevicesManager(int maximumNumberOfTryToConnect, ThreadPoolExecutor threadPoolExecutor, BigFinCommandEngine commandEngine) {
        this.maximumNumberOfTryToConnect = maximumNumberOfTryToConnect;
        this.threadPoolExecutor = threadPoolExecutor;
        this.commandEngine = commandEngine;
    }

    /**
     * Get the list of remote device names.
     *
     * @param reload flag to reload remote devices
     * @return list of remote device names
     * @throws RemoteDeviceNotFoundException if could not find remote devices
     */
    public List<String> listDevices(boolean reload) throws RemoteDeviceNotFoundException {
        if (reload) {
            clearRemoteDeviceCaches();
        }
        List<String> list = new ArrayList<>(getRemoteDevices().keySet());
        list.sort(String::compareTo);
        return list;
    }

    /**
     * Open a remote device for the given {@code name}.
     *
     * @param name name of remote device to open
     * @return the remove device ready to be used
     */
    public BigFinDevice openDevice(String name) {
        Objects.requireNonNull(name, "No remote device name given");
        closeOpenDevice();
        Map<String, RemoteDevice> remoteDevices = getRemoteDevices();
        if (remoteDevices.isEmpty()) {
            throw new RemoteDeviceNotFoundException("No remote device found");
        }
        RemoteDevice remoteDevice = remoteDevices.get(name);
        if (remoteDevice == null) {
            throw new RemoteDeviceNotFoundException(String.format("Could not find remote device with name '%s'", name));
        }
        Map<String, String> remoteConnectionUrlCache = getRemoteConnectionUrlCache();
        if (!remoteConnectionUrlCache.containsKey(name)) {
            int serviceIndex = 3;
            ServiceRecordsFinder serviceRecordsFinder = new ServiceRecordsFinder(maximumNumberOfTryToConnect, this);
            List<ServiceRecord> serviceRecords;
            try {
                serviceRecords = serviceRecordsFinder.findServices(getLocalDevice(), remoteDevice, serviceIndex);
                if (serviceRecords.isEmpty()) {
                    throw new RemoteDeviceServiceNotFoundException("No service detected.");
                }
            } catch (Exception e) {
                throw new RemoteDeviceNotFoundException("Could not read remote device services", e);
            }
            log.info(String.format("Found some services for index: %d", serviceIndex));
            // take first service record
            ServiceRecord serviceRecord = serviceRecords.get(0);
            // get connection url
            String url = serviceRecord.getConnectionURL(ServiceRecord.AUTHENTICATE_ENCRYPT, false);
            log.info(String.format("Found service(%d): %s, name: %s", serviceIndex, url, name));
            remoteConnectionUrlCache.put(name, url);
        }
        String connectionUrl = Objects.requireNonNull(remoteConnectionUrlCache.get(name), String.format("Can not find url for remote device: %s", name));
        return openDevice = new BigFinDevice(threadPoolExecutor, commandEngine, name, connectionUrl);
    }

    /**
     * Open a remote device for the given {@code name}.
     *
     * @param name name of remote device to open
     * @param connectionUrl url connection of remote device to open
     * @return the remove device ready to be used
     */
    public BigFinDevice openDeviceFromUrl(String name, String connectionUrl) {
        Objects.requireNonNull(name, "No remote device name given");
        Objects.requireNonNull(connectionUrl, "No remote url given");
        closeOpenDevice();
        Map<String, RemoteDevice> remoteDevices = getRemoteDevices();
        if (remoteDevices.isEmpty()) {
            throw new RemoteDeviceNotFoundException("No remote device found");
        }
        RemoteDevice remoteDevice = remoteDevices.get(name);
        if (remoteDevice == null) {
            throw new RemoteDeviceNotFoundException(String.format("Could not find remote device with name '%s'", name));
        }
        Map<String, String> remoteConnectionUrlCache = getRemoteConnectionUrlCache();
        if (!remoteConnectionUrlCache.containsKey(name)) {
            remoteConnectionUrlCache.put(name, connectionUrl);
        }
        return openDevice = new BigFinDevice(threadPoolExecutor, commandEngine, name, connectionUrl);
    }

    @Override
    public void close() {
        try {
            if (openDevice != null && !openDevice.isClosed()) {
                openDevice.close();
            }
            clearRemoteDeviceCaches();
        } finally {
            BlueCoveImpl.shutdown();
        }
    }

    /**
     * Close open device if it exists.
     */
    public void closeOpenDevice() {
        if (openDevice == null) {
            log.warn("No open device found.");
            return;
        }
        if (!openDevice.isClosed()) {
            openDevice.close();
        }
        openDevice = null;
    }

    private void clearRemoteDeviceCaches() {
        remoteDevices = null;
        remoteConnectionUrlCache = null;
    }

    private LocalDevice getLocalDevice() throws LocalDeviceNotFoundException {
        if (localDevice == null) {
            try {
                localDevice = LocalDevice.getLocalDevice();
            } catch (BluetoothStateException e) {
                throw new LocalDeviceNotFoundException();
            }
        }
        return localDevice;
    }

    private Map<String, RemoteDevice> getRemoteDevices() throws RemoteDeviceNotFoundException {
        if (remoteDevices == null) {
            RemoteDevicesFinder remoteDevicesFinder = new RemoteDevicesFinder(maximumNumberOfTryToConnect, this);
            try {
                remoteDevices = remoteDevicesFinder.findDevices(getLocalDevice());
            } catch (Exception e) {
                throw new RemoteDeviceNotFoundException("Could not detected devices", e);
            }
        }
        return remoteDevices;
    }

    private Map<String, String> getRemoteConnectionUrlCache() {
        if (remoteConnectionUrlCache == null) {
            remoteConnectionUrlCache = new TreeMap<>();
        }
        return remoteConnectionUrlCache;
    }

    static class RemoteDevicesFinder {

        private final Map<String, RemoteDevice> devices;
        private final int maximumNumberOfTryToConnect;
        private final Object lock;

        RemoteDevicesFinder(int maximumNumberOfTryToConnect, Object lock) {
            this.maximumNumberOfTryToConnect = maximumNumberOfTryToConnect;
            this.lock = lock;
            this.devices = new TreeMap<>();
        }

        Map<String, RemoteDevice> findDevices(LocalDevice localDevice) throws BluetoothStateException, InterruptedException {
            DevicesDiscoveryListener listener = new DevicesDiscoveryListener();
//            synchronized (lock) {
                int tryRound = 0;
                while (tryRound < maximumNumberOfTryToConnect && devices.isEmpty()) {
                    boolean started = localDevice.getDiscoveryAgent().startInquiry(DiscoveryAgent.LIAC, listener);
                    if (started) {
                        log.info(String.format("Wait for device inquiry to complete... (try %d)", tryRound));
                        synchronized (lock) {
                            lock.wait();
                        }
                        log.info(String.format("%d device(s) found", devices.size()));
                    }
                    tryRound++;
                }
//            }
            return devices;
        }

        protected class DevicesDiscoveryListener implements DiscoveryListener {

            @Override
            public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
                log.info(String.format("Device %s found", btDevice.getBluetoothAddress()));
                String friendlyName = null;
                int tryRound = 0;
                while (tryRound < maximumNumberOfTryToConnect * 3 && friendlyName == null) {
                    log.info(String.format("Try to get device friendlyName (try %d)", tryRound));
                    friendlyName = getName(btDevice);
                    tryRound++;
                }
                if (friendlyName == null) {
                    throw new RemoteDeviceCantGetNameException(btDevice.getBluetoothAddress());
                }
                if (friendlyName.contains("Big") || friendlyName.contains("Fin")) {
                    devices.put(friendlyName, btDevice);
                }
            }

            String getName(RemoteDevice btDevice) {
                String friendlyName = null;
                try {
                    friendlyName = btDevice.getFriendlyName(false);
                    log.info(String.format("Name: %s", friendlyName));
                } catch (IOException e) {
                    log.debug("Can't get name of remote", e);
                }
                return friendlyName;
            }

            @Override
            public void inquiryCompleted(int discType) {
                log.info("Device Inquiry completed!");
                synchronized (lock) {
                    lock.notifyAll();
                }
            }

            @Override
            public void serviceSearchCompleted(int transID, int respCode) {
                log.info("serviceSearchCompleted");
            }

            @Override
            public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
                log.info("servicesDiscovered");
            }
        }
    }

    static class ServiceRecordsFinder {

        private final int maximumNumberOfTryToConnect;
        private final Object lock;
        private final List<ServiceRecord> serviceRecords;

        ServiceRecordsFinder(int maximumNumberOfTryToConnect, Object lock) {
            this.maximumNumberOfTryToConnect = maximumNumberOfTryToConnect;
            this.lock = lock;
            this.serviceRecords = new ArrayList<>();
        }

        List<ServiceRecord> findServices(LocalDevice localDevice, RemoteDevice device, int serviceIndex) throws InterruptedException, BluetoothStateException {
            ServicesDiscoveryListener listener = new ServicesDiscoveryListener();
            int tryRound = 0;
            while (tryRound < maximumNumberOfTryToConnect*3 && serviceRecords.isEmpty()) {
                synchronized (lock) {
                    int[] attrIDs = new int[]{0x0100 /* Service name*/};
                    log.info("Trying to get services (try " + tryRound + ")");
                    localDevice.getDiscoveryAgent().searchServices(attrIDs,
                            new UUID[]{new UUID(serviceIndex)},
                            device,
                            listener);
                    lock.wait();
                }
                tryRound++;
            }
            return serviceRecords;
        }

        String getServiceName(ServiceRecord serviceRecord) {
            DataElement serviceName = serviceRecord.getAttributeValue(0x0100);
            String result;
            if (serviceName == null) {
                result = null;
            } else {
                result = String.valueOf(serviceName.getValue());
            }
            return result;

        }

        protected class ServicesDiscoveryListener implements DiscoveryListener {

            @Override
            public void serviceSearchCompleted(int transID, int respCode) {
                log.debug("Service search completed!");
                synchronized (lock) {
                    lock.notifyAll();
                }
            }

            @Override
            public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
                for (ServiceRecord aServRecord : servRecord) {
                    String url = aServRecord.getConnectionURL(ServiceRecord.AUTHENTICATE_ENCRYPT, true);
                    if (url == null) {
                        continue;
                    }
                    serviceRecords.add(aServRecord);
                    String serviceName = getServiceName(aServRecord);
                    log.debug("service found " + url + (serviceName == null ? "" : ", name: " + serviceName));
                }
            }

            @Override
            public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
                log.info("deviceDiscovered");
            }

            @Override
            public void inquiryCompleted(int discType) {
                log.info("inquiryCompleted");
            }
        }
    }
}
