package io.ultreia.ifremer.cali.api.io;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

/**
 * Created on 1/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BigFinCommand {

    private final String question;
    private final boolean needResponse;
    private String response;

    public BigFinCommand(String question) {
        this(question, false);
    }

    public BigFinCommand(String question, boolean needReponse) {
        this.question = Objects.requireNonNull(question);
        this.needResponse = needReponse;
    }

    public boolean isNeedResponse() {
        return needResponse;
    }

    public boolean isResponseComplete(String response) {
        return false;
    }

    public String getQuestion() {
        return question;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "BigFinCommandResult{" +
                "question='" + question + '\'' +
                (response == null ? "response=none" : "response='" + response + '\'') +
                '}';
    }

}
