package io.ultreia.ifremer.cali.api.io.command;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.io.BigFinCommand;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 12/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GetTemperatureAndHumidity extends BigFinCommand {

    private static final Log log = LogFactory.getLog(GetTemperatureAndHumidity.class);
    private static final String REQUEST = "&t#";
    private static final String RESPONSE_PREFIX = "$t,";
    private Integer temperature;
    private Integer humidity;

    public GetTemperatureAndHumidity() {
        super(REQUEST, true);
    }

    @Override
    public boolean isResponseComplete(String response) {
        return response == null || response.trim().startsWith(RESPONSE_PREFIX);
    }

    @Override
    public void setResponse(String response) {
        super.setResponse(response);
        if (response != null) {
            String[] parts = StringUtils.removeStart(response.trim(), RESPONSE_PREFIX).trim().replace("#", "").split(",");
            try {
                temperature = Integer.valueOf(parts[0]);
            } catch (Exception e) {
                log.error("Can't get temperature from response: " + response, e);
                temperature = null;
            }
            try {
                humidity = Integer.valueOf(parts[1]);
            } catch (Exception e) {
                log.error("Can't get humidity from response: " + response, e);
                humidity = null;
            }
        }
    }

    public Integer getTemperature() {
        return temperature;
    }

    public Integer getHumidity() {
        return humidity;
    }
}
