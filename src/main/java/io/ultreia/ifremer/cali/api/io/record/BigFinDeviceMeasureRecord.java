package io.ultreia.ifremer.cali.api.io.record;

/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A measure record.
 * <p>
 * Format is <pre>%l,v#</pre>.
 * <p>
 * Created on 12/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class BigFinDeviceMeasureRecord extends BigFinDeviceRecordSupport {

    protected static final Pattern RECORD_PATTERN = Pattern.compile("%l,(\\w+)?#");
    private static final long serialVersionUID = 1L;
    protected final int measure;

    BigFinDeviceMeasureRecord(String record, Date date) {
        super(record, date);
        Matcher matcher = RECORD_PATTERN.matcher(record);
        matcher.find();
        String measureStr = matcher.group(1);
        this.measure = Integer.valueOf(measureStr);
    }

    public static boolean acceptRecord(String record) {

        Matcher matcher = RECORD_PATTERN.matcher(record);
        return matcher.matches();

    }

    public int getMeasure() {
        return measure;
    }

    @Override
    public boolean isValid() {
        return acceptRecord(getRecord());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("record", getRecord())
                .append("measure", getMeasure())
                .append("date", getDate())
                .toString();
    }

}
