package io.ultreia.ifremer.cali.ui.board;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.Board;
import io.ultreia.ifremer.cali.api.BoardApi;
import io.ultreia.ifremer.cali.api.Measure;
import io.ultreia.ifremer.cali.ui.calibration.CalibrationUIModel;
import io.ultreia.ifremer.cali.ui.measure.MeasureUIModel;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class BoardUIModel extends Board {

    private final CalibrationUIModel calibration;
    private final MeasureUIModel measure;
    private boolean measureMode;
    private boolean calibrationMode;

    public BoardUIModel() {
        this.calibration = new CalibrationUIModel(this);
        this.measure = new MeasureUIModel(this);
    }

    public String getBatteryLevelValue() {
        Integer batteryLevel = getBatteryLevel();
        return batteryLevel == null ? t("cali.board.unknownValue") : batteryLevel + " %";
    }

    public String getHumidityValue() {
        Integer humidity = getHumidity();
        return humidity == null ? t("cali.board.unknownValue") : humidity + " %";
    }

    public String getLastUpdateValue() {
        Date lastUpdate = getLastUpdate();
        return lastUpdate == null ? t("cali.board.unknownValue") : BoardApi.DATE_FORMAT.format(lastUpdate);
    }

    public String getTemperatureValue() {
        Integer temperature = getTemperature();
        return temperature == null ? t("cali.board.unknownValue") : temperature + " °";
    }

    @Override
    public void setBatteryLevel(Integer batteryLevel) {
        super.setBatteryLevel(batteryLevel);
        firePropertyChange("batteryLevelValue", null, getBatteryLevelValue());
    }

    @Override
    public void setHumidity(Integer humidity) {
        super.setHumidity(humidity);
        firePropertyChange("humidityValue", null, getHumidityValue());
    }

    @Override
    public void setLastUpdate(Date lastUpdate) {
        super.setLastUpdate(lastUpdate);
        firePropertyChange("lastUpdateValue", null, getLastUpdateValue());
    }

    @Override
    public void setTemperature(Integer temperature) {
        super.setTemperature(temperature);
        firePropertyChange("temperatureValue", null, getTemperatureValue());
    }

    public boolean isMeasureMode() {
        return measureMode;
    }

    public void setMeasureMode(boolean measureMode) {
        boolean oldValue = isMeasureMode();
        this.measureMode = measureMode;
        firePropertyChange("measureMode", oldValue, measureMode);
    }

    public boolean isCalibrationMode() {
        return calibrationMode;
    }

    public void setCalibrationMode(boolean calibrationMode) {
        boolean oldValue = isCalibrationMode();
        this.calibrationMode = calibrationMode;
        firePropertyChange("calibrationMode", oldValue, calibrationMode);
    }

    public CalibrationUIModel getCalibration() {
        return calibration;
    }

    public MeasureUIModel getMeasure() {
        return measure;
    }

    public void startMeasureMode(Measure measure) {
        getMeasure().clearMeasures();
        measure.copy(getMeasure());
        setMeasureMode(true);
    }
}
