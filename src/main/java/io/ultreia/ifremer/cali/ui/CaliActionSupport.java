package io.ultreia.ifremer.cali.ui;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.CaliContext;
import org.apache.commons.lang3.SystemUtils;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.ApplicationAction;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created by tchemit on 17/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class CaliActionSupport<UI extends JAXXObject> extends ApplicationAction<UI> {

    private static final KeyStroke PRESSED_ENTER = KeyStroke.getKeyStroke("pressed ENTER");

    protected CaliActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    protected abstract String getActionTitle();

    public abstract void run();

    @Override
    protected final void doActionPerformed(ActionEvent e, UI ui) {
        if (this instanceof Runnable)
            CaliContext.context().addAction(Objects.requireNonNull(getActionTitle()), (Runnable) this);
        else {
            run();
            CaliContext.context().displayInfo(Objects.requireNonNull(getActionTitle()));
        }
    }

    @Override
    public void init() {
        defaultInit(null, null);
        installAction(ui instanceof JComponent ? (JComponent) ui : ((JFrame) ui).getRootPane());
        if (editor != null) {
            editor.setForeground(Color.BLUE);
            editor.addPropertyChangeListener("enabled", e -> {
                boolean newValue = (boolean) e.getNewValue();
                editor.setForeground(newValue ? Color.BLUE : Color.LIGHT_GRAY);
            });
            if (SystemUtils.IS_OS_WINDOWS) {
                String actionMapKey = getActionCommandKey() + ".enter";
                editor.getInputMap(JComponent.WHEN_FOCUSED).put(PRESSED_ENTER, actionMapKey);
                editor.getActionMap().put(actionMapKey, this);
            }
        }
    }

    public void installAction(JComponent ui) {
        register(ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW), ui.getActionMap());
    }


}
