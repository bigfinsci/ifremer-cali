package io.ultreia.ifremer.cali.ui.measure;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import io.ultreia.ifremer.cali.api.BoardApi;
import io.ultreia.ifremer.cali.api.Measure;
import io.ultreia.ifremer.cali.api.io.record.BigFinDeviceMeasureRecord;
import io.ultreia.ifremer.cali.api.io.record.BigFinDeviceRecordEvent;
import io.ultreia.ifremer.cali.api.io.record.BigFinDeviceRecordListener;
import io.ultreia.ifremer.cali.ui.board.BoardUIModel;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.table.AbstractTableModel;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 14/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class MeasureUIModel extends Measure implements BigFinDeviceRecordListener {

    private static final Log log = LogFactory.getLog(MeasureUIModel.class);
    private final MeasureTableModel dataModel;

    public MeasureUIModel(BoardUIModel board) {
        super(board);
        this.dataModel = new MeasureTableModel(this);
    }

    public MeasureTableModel tableModel() {
        return dataModel;
    }

    public void clearMeasures() {
        int measuresCount = getMeasuresCount();
        getMeasures().clear();
        if (measuresCount > 0)
            dataModel.fireTableRowsDeleted(0, measuresCount - 1);
        firePropertyChange("measuresCount", null, getMeasuresCount());
    }

    @Override
    public BoardUIModel getBoard() {
        return (BoardUIModel) super.getBoard();
    }


    @Override
    public void recordRead(BigFinDeviceRecordEvent event) {
        BigFinDeviceMeasureRecord lastMeasure = event.getRecord();
        getMeasures().add(lastMeasure);
        dataModel.addMeasure(lastMeasure);
        setLastMeasure(lastMeasure);
        firePropertyChange("measuresCount", null, getMeasuresCount());
    }

    public String getMeasuresToClipBoard() {
        return Joiner.on("\n").join(getMeasures().stream().map(BigFinDeviceMeasureRecord::getMeasure).collect(Collectors.toList()));
    }

    private class MeasureTableModel extends AbstractTableModel {

        private final MeasureUIModel model;

        private MeasureTableModel(MeasureUIModel model) {
            this.model = model;
        }

        @Override
        public int getRowCount() {
            return model.getMeasuresCount();
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return t("cali.measure.date");
                case 1:
                    return t("cali.measure.measure");
                default:
                    throw new IllegalStateException();
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return String.class;
                case 1:
                    return int.class;
                default:
                    throw new IllegalStateException();
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            BigFinDeviceMeasureRecord record = model.getMeasures().get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return BoardApi.DATE_FORMAT.format(record.getDate());
                case 1:
                    return record.getMeasure();
            }
            return record;
        }

        public void addMeasure(BigFinDeviceMeasureRecord newValue) {
            if (newValue == null) {
                return;
            }
            log.info(String.format("New measure added in table model: %s", newValue));
            int rowCount = getRowCount() - 1;
            fireTableRowsInserted(rowCount, rowCount);
        }
    }
}
