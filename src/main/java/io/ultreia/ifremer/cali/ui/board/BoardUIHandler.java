package io.ultreia.ifremer.cali.ui.board;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import io.ultreia.ifremer.cali.CaliContext;
import io.ultreia.ifremer.cali.RunCali;
import io.ultreia.ifremer.cali.api.Board;
import io.ultreia.ifremer.cali.ui.CaliActionSupport;
import io.ultreia.ifremer.cali.ui.main.MainUI;
import io.ultreia.java4all.lang.Strings;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializerManager;
import org.nuiton.jaxx.runtime.swing.ApplicationAction;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class BoardUIHandler implements UIHandler<BoardUI> {

    private final BoardUI ui;
    private Set<JComponent> measureComponents;
    private Set<JComponent> calibrationComponents;

    BoardUIHandler(BoardUI ui) {
        this.ui = ui;
    }

    @Override
    public void beforeInit(BoardUI boardUI) {
        BoardUIModel model = new BoardUIModel();
        ui.setContextValue(model);
        ui.setContextValue(model.getCalibration());
        ui.setContextValue(model.getMeasure());
    }

    @Override
    public void afterInit(BoardUI ui) {
        ComponentInitializerManager.get().apply(this.ui);
        ui.getModel().addPropertyChangeListener("batteryLevel", e -> updateBatteryLevel((Integer) e.getNewValue()));
        ui.getModel().addPropertyChangeListener("calibrationMode", e -> updateCalibrationMode((boolean) e.getNewValue()));
        ui.getModel().addPropertyChangeListener("humidity", e -> updateHumidity((Integer) e.getNewValue()));
        ui.getModel().addPropertyChangeListener("lastUpdate", e -> updateLastUpdate((Date) e.getNewValue()));
        ui.getModel().addPropertyChangeListener("measureMode", e -> updateMeasureMode((boolean) e.getNewValue()));
        ui.getModel().addPropertyChangeListener("temperature", e -> updateTemperature((Integer) e.getNewValue()));

        calibrationComponents = ImmutableSet.of(ui.calibrationModeLabel, ui.calibration, ui.calibrationModeSeparator);
        measureComponents = ImmutableSet.of(ui.measureModeLabel, ui.measure, ui.measureModeSeparator);

        ui.getModel().getMeasure().addPropertyChangeListener("measuresCount", e -> onMeasureCountChanged((int) e.getNewValue()));
        ui.getModel().getCalibration().addPropertyChangeListener("valid", e -> onCalibrationValidChanged((boolean) e.getNewValue()));

        updateBatteryLevel(ui.getModel().getBatteryLevel());
        updateCalibrationMode(false);
        updateHumidity(ui.getModel().getHumidity());
        updateLastUpdate(ui.getModel().getLastUpdate());
        updateMeasureMode(false);
        updateTemperature(ui.getModel().getTemperature());
    }

    private void onCalibrationValidChanged(boolean newValue) {
        ui.getCalibrationModeLabel().setText(newValue ? t("cali.board.calibration.done") : t("cali.board.calibration.progress"));
        ui.getCalibrationModeLabel().setForeground(newValue ? RunCali.OK_COLOR : RunCali.WAITING_COLOR);
    }

    void close() {
        ApplicationAction.run(ui, DisconnectBoard.class);
    }

    void updateModel(Board board) {
        ui.getModel().setCalibrationMode(false);
        ui.getModel().setMeasureMode(false);
        board.copy(ui.getModel());
        ((CaliActionSupport) ui.getDisconnectBoard().getAction()).installAction(ui);
    }

    void disconnectSelectedBoard() {
        BoardUIModel selectedBoard = ui.getModel();
        ui.getContext().addAction(t("cali.board.disconnectBoard.title", selectedBoard.getBoardName()), new DisconnectSelectedBoard(selectedBoard));
    }

    private void updateMeasureMode(boolean newValue) {
        measureComponents.forEach(c -> c.setVisible(newValue));
        JComponent focus;
        if (newValue) {
            ui.getMeasure().getHandler().start(ui);
            focus = ui.getMeasure().getStopMeasure();
        } else {
            focus = onReturnToBoard();
        }
        ui.getBoardActions().setVisible(!newValue);
        ui.getBoardActionsSeparator().setVisible(!newValue);
        SwingUtilities.invokeLater(focus::requestFocus);
    }

    private void updateCalibrationMode(boolean newValue) {
        calibrationComponents.forEach(c -> c.setVisible(newValue));
        JComponent focus;
        if (newValue) {
            ui.getCalibration().getHandler().start(ui);
            focus = ui.getCalibration().getCancelOrQuit();
        } else {
            focus = onReturnToBoard();
        }
        ui.getBoardActions().setVisible(!newValue);
        ui.getBoardActionsSeparator().setVisible(!newValue);
        SwingUtilities.invokeLater(focus::requestFocus);
    }

    private JComponent onReturnToBoard() {
        JComponent focus;
        ((CaliActionSupport) ui.getStartCalibrationMode().getAction()).installAction(ui);
        ((CaliActionSupport) ui.getStartMeasureMode().getAction()).installAction(ui);
        ((CaliActionSupport) ui.getRefreshValues().getAction()).installAction(ui);
        focus = ui.getStartCalibrationMode();
        return focus;
    }

    private void updateBatteryLevel(Integer newValue) {
        ui.getBatteryLevel().setForeground(newValue == null ? RunCali.WAITING_COLOR : RunCali.OK_COLOR);
    }

    private void updateHumidity(Integer newValue) {
        if (newValue == null) {
            ui.getHumidity().setForeground(RunCali.WAITING_COLOR);
        } else if (newValue > 39) {
            ui.getHumidity().setForeground(RunCali.KO_COLOR);
        } else {
            ui.getHumidity().setForeground(RunCali.OK_COLOR);
        }
    }

    private void updateLastUpdate(Date newValue) {
        ui.getLastUpdate().setForeground(newValue == null ? RunCali.WAITING_COLOR : RunCali.OK_COLOR);
    }

    private void updateTemperature(Integer newValue) {
        ui.getTemperature().setForeground(newValue == null ? RunCali.WAITING_COLOR : RunCali.OK_COLOR);
    }

    private void onMeasureCountChanged(int size) {
        String labelText;
        Color color;
        if (size == 0) {
            labelText = t("cali.board.measureMode.none");
            color = RunCali.WAITING_COLOR;
        } else if (size == 1) {
            labelText = t("cali.board.measureMode.one");
            color = RunCali.OK_COLOR;
        } else {
            labelText = t("cali.board.measureMode.more", size);
            color = RunCali.OK_COLOR;
        }
        ui.getMeasureModeLabel().setText(labelText);
        ui.getMeasureModeLabel().setForeground(color);
    }

    private static class DisconnectSelectedBoard implements Runnable {
        private final BoardUIModel selectedBoard;

        DisconnectSelectedBoard(BoardUIModel selectedBoard) {
            this.selectedBoard = Objects.requireNonNull(selectedBoard);
        }

        @Override
        public void run() {
            long t0 = System.nanoTime();

            CaliContext context = CaliContext.context();
            MainUI ui = context.getMainUI();
            if (selectedBoard.isCalibrationMode()) {
                selectedBoard.getCalibration().stop();
            }
            context.getBoardApi().disconnect(selectedBoard);

            String time = Strings.convertTime(t0, System.nanoTime());
            ui.displayInfo(t("cali.board.disconnectBoard.done", selectedBoard.getBoardName(), time));

            ui.getModel().setSelectedBoard(null);
            context.unlock();
        }
    }
}
