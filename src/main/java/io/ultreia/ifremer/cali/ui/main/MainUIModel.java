package io.ultreia.ifremer.cali.ui.main;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.Board;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tchemit on 09/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition(predicate = false, comparator = false, stream = false, instance = false)
public class MainUIModel extends AbstractJavaBean {

    public static final String DETECTED_BOARDS = "detectedBoards";
    public static final String SELECTED_BOARD = "selectedBoard";
    private final List<String> detectedBoards;
    private Board selectedBoard;

    public MainUIModel() {
        detectedBoards = new LinkedList<>();
    }

    public boolean isDetectedBoardsEmpty() {
        return detectedBoards.isEmpty();
    }

    public List<String> getDetectedBoards() {
        return detectedBoards;
    }

    public void setDetectedBoards(List<String> detectedBoards) {
        this.detectedBoards.clear();
        this.detectedBoards.addAll(detectedBoards);
        firePropertyChange(DETECTED_BOARDS, detectedBoards);
        firePropertyChange("detectedBoardsEmpty", isDetectedBoardsEmpty());
    }

    public boolean isSelectedBoardPresent() {
        return selectedBoard != null;
    }

    public Board getSelectedBoard() {
        return selectedBoard;
    }

    public void setSelectedBoard(Board selectedBoard) {
        Board oldValue = getSelectedBoard();
        this.selectedBoard = selectedBoard;
        firePropertyChange(SELECTED_BOARD, oldValue, selectedBoard);
        firePropertyChange("selectedBoardPresent", null, isSelectedBoardPresent());
    }

}
