/*
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.ifremer.cali.ui.main;

import io.ultreia.ifremer.cali.CaliConfig;
import io.ultreia.ifremer.cali.CaliContext;
import io.ultreia.ifremer.cali.RunCali;
import io.ultreia.ifremer.cali.api.Board;
import io.ultreia.ifremer.cali.api.BoardApi;
import io.ultreia.ifremer.cali.ui.board.BoardUI;
import io.ultreia.ifremer.cali.ui.board.DisconnectBoard;
import io.ultreia.ifremer.cali.ui.main.menu.MenuFileCloseApplication;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.application.UserMessages;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializerManager;
import org.nuiton.jaxx.runtime.swing.ApplicationAction;
import org.nuiton.jaxx.runtime.swing.BlockingLayerUI;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;

import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * {@link MainUI} handler.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see MainUI
 * @since 1.0
 */
public final class MainUIHandler implements UIHandler<MainUI> {

    private static final Log log = LogFactory.getLog(MainUIHandler.class);
    private final MainUI ui;
    private final BlockingLayerUI blockingLayerUI;

    MainUIHandler(MainUI ui) {
        this.ui = ui;
        this.blockingLayerUI = new BlockingLayerUI();
        blockingLayerUI.setUseIcon(false);
        blockingLayerUI.setAcceptedComponentNames("cancelOrQuit", "startMeasureMode");
        blockingLayerUI.setBlockingColor(UIManager.getColor("BlockingLayerUI.blockingColor"));
        UIManager.put("ComboBox.noActionOnKeyNavigation", true);
        ui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onWindowClosing();
            }
        });
    }

    @Override
    public void afterInit(MainUI ui) {
        ComponentInitializerManager.get().apply(this.ui);
        CaliConfig config = CaliConfig.configuration();
        String title = String.format("%s v %s", config.getApplicationName(), config.getApplicationVersion());
        ui.setTitle(title);
        ui.getStatus().init();

        SwingUtil.setLayerUI(ui.getBody(), blockingLayerUI);

        ui.getConfig().addPropertyChangeListener(CaliConfig.PROPERTY_BUSY, evt -> {
            boolean busy = (boolean) evt.getNewValue();
            if (busy) {
                log.debug("block ui in busy mode");
                ui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                blockingLayerUI.setBlock(true);
            } else {
                log.debug("unblock ui in none busy mode");
                ui.setCursor(Cursor.getDefaultCursor());
                blockingLayerUI.setBlock(false);
            }
        });
        ui.getModel().addPropertyChangeListener(MainUIModel.DETECTED_BOARDS, this::updateDetectedBoards);
        ui.getModel().addPropertyChangeListener(MainUIModel.SELECTED_BOARD, e->updateSelectedBoard((Board) e.getNewValue()));
        ui.getBoards().addItemListener(this::updatedSelectedBoardName);
        Locale locale = config.getApplicationLocale();
        ui.getMenuConfigurationToEnglish().setSelected(Locale.UK.equals(locale));
        ui.getMenuConfigurationToFrench().setSelected(Locale.FRANCE.equals(locale));
        updateBoardsTitle(true, Collections.emptyList());
        updateSelectBoardPanel();
        updateSelectedBoard(null);
    }

    private void onWindowClosing() {
        ApplicationAction.run(ui, MenuFileCloseApplication.class);
    }

    void close() {
        if (!ui.getModel().isSelectedBoardPresent()) {
            return;
        }
        ui.getBoard().close();
    }

    private void updateDetectedBoards(PropertyChangeEvent event) {
        log.debug("Detected boards changed: " + event);
        List<String> boards = ui.getModel().getDetectedBoards();

        Board selectedBoard = ui.getModel().getSelectedBoard();
        boolean removeBoard = selectedBoard != null && !boards.contains(selectedBoard.getBoardName());

        SwingUtil.fillComboBox(ui.getBoards(), boards, removeBoard || selectedBoard == null ? null : selectedBoard.getBoardName());
        updateBoardsTitle(false, boards);

        updateSelectBoardPanel();

        if (removeBoard) {
            ApplicationAction.run(ui.getBoard(), DisconnectBoard.class);
        }
    }

    private void updatedSelectedBoardName(ItemEvent event) {
        log.debug("Selected board name changed: " + event);
        if (event.getStateChange() == ItemEvent.SELECTED) {
            String boardName = (String) event.getItem();
            if (!ui.getModel().isSelectedBoardPresent() || !boardName.equals(ui.getModel().getSelectedBoard().getBoardName())) {
                ui.getContext().addAction(t("cali.main.connectBoard.title", boardName), new ConnectSelectedBoard(boardName));
            }
        }
    }

    private void updateSelectedBoard(Board newValue) {
        log.debug("Selected board changed: " + newValue);
        if (newValue == null) {
            ui.getBoardsPanel().setVisible(true);
            ui.getSelectBoardPanel().setVisible(!ui.getModel().isDetectedBoardsEmpty());
            ui.getBoards().setSelectedIndex(-1);
            ui.getSelectedBoardPanel().setVisible(false);
            ui.getSelectBoard().requestFocus();
        } else {
            ui.getBoard().getSelectedBoardLabel().setText(t("cali.board.selectedBoard", newValue.getBoardName()));
            BoardUI boardUI = ui.getBoard();
            boardUI.updateModel(newValue);
            ui.getBoardsPanel().setVisible(false);
            ui.getSelectBoardPanel().setVisible(false);
            ui.getSelectedBoardPanel().setVisible(true);
            ui.getBoard().getStartCalibrationMode().requestFocus();
        }
    }

    private void updateSelectBoardPanel() {
        boolean empty = ui.getModel().isDetectedBoardsEmpty();
        ui.getSelectBoardPanel().setVisible(!empty);
        ui.getSelectedBoardPanel().setVisible(!empty && ui.getModel().isSelectedBoardPresent());
        if (empty) {
            ui.getGetBoards().requestFocus();
        } else {
            ui.getSelectBoard().setPreferredSize(ui.getGetBoards().getPreferredSize());
            ui.getSelectBoard().setSize(ui.getGetBoards().getSize());
            ui.getSelectBoard().requestFocus();
        }
    }

    private void updateBoardsTitle(boolean first, List<String> boards) {
        int size = boards.size();
        String title;
        Color titleColor;
        if (size == 0) {
            if (first) {
                titleColor = RunCali.WAITING_COLOR;
                title = t("cali.main.boards.unknown");
            } else {
                titleColor = Color.RED;
                title = t("cali.main.boards.foundNone");
            }
        } else {
            if (size == 1) {
                title = t("cali.main.boards.foundOne");
            } else {
                title = t("cali.main.boards.foundMore", size);
            }
            titleColor = RunCali.OK_COLOR;
        }
        ui.getBoardsLabel().setText(title);
        if (first) {
            ui.getBoardsDetectionLabel().setText(t("cali.main.boards.lastDate.unknown"));
        } else {
            ui.getBoardsDetectionLabel().setText(t("cali.main.boards.lastDate.found", BoardApi.DATE_FORMAT.format(new Date())));
        }
        ui.getBoardsLabel().setForeground(titleColor);
    }

    public static class ConnectSelectedBoard implements Runnable {
        private final String selectedBoard;

        ConnectSelectedBoard(String selectedBoard) {
            this.selectedBoard = selectedBoard;
        }

        @Override
        public void run() {
            long t0 = System.nanoTime();
            CaliContext context = CaliContext.context();
            MainUI ui = context.getMainUI();
            MainUIModel model = ui.getModel();
            if (model.isSelectedBoardPresent()) {
                Board selectedBoard = model.getSelectedBoard();
                try {
                    context.getBoardApi().disconnect(selectedBoard);
                } catch (Exception e) {
                    UserMessages.displayWarning(t("cali.error.title"), t("cali.board.disconnectBoard.error", selectedBoard.getBoardName()));
                    ui.getBoards().setSelectedIndex(-1);
                }
            }
            try {
                Board board = context.getBoardApi().connect(selectedBoard);
                model.setSelectedBoard(board);
                String time = Strings.convertTime(t0, System.nanoTime());
                ui.displayInfo(t("cali.main.connectBoard.done", selectedBoard, time));
            } catch (Exception e) {
                UserMessages.displayWarning(t("cali.error.title"), t("cali.main.connectBoard.error", selectedBoard));
                ui.getBoards().setSelectedIndex(-1);
            }
            context.unlock();
        }
    }

    public static class MainUIReloadUI implements Runnable {
        private final boolean replace;

        public MainUIReloadUI(boolean replace) {
            this.replace = replace;
        }

        @Override
        public void run() {
            CaliContext rootContext = CaliContext.context();
            CaliConfig config = rootContext.getConfig();
            config.removeJaxxPropertyChangeListener();
            ErrorDialogUI.init(null);
            UserMessages.setMainUI(null);
            rootContext.removeMainUI();

            MainUI ui = rootContext.newMainUI();
            UserMessages.setMainUI(ui);
            ErrorDialogUI.init(ui);
            rootContext.setMainUIVisible(ui, replace);
        }
    }

}
