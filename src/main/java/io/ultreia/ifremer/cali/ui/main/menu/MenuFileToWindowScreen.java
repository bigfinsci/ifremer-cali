package io.ultreia.ifremer.cali.ui.main.menu;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.ui.main.MainUI;
import io.ultreia.ifremer.cali.ui.main.MainUIHandler;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class MenuFileToWindowScreen extends MenuActionSupport {

    public MenuFileToWindowScreen() {
        super(t("cali.action.toWindowScreen"), t("cali.action.toWindowScreen.tip"), "window-screen", 'S');
    }

    @Override
    protected void doActionPerformed0(ActionEvent event, MainUI ui) {
        ui.getConfig().setFullScreen(false);
        new MainUIHandler.MainUIReloadUI(true).run();
    }
}
