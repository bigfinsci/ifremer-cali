package io.ultreia.ifremer.cali.ui.calibration;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.api.Measure;
import io.ultreia.ifremer.cali.ui.CaliActionSupport;
import io.ultreia.ifremer.cali.ui.board.BoardUIModel;

import javax.swing.KeyStroke;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 09/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class StartMeasureMode extends CaliActionSupport<CalibrationUI> {

    public StartMeasureMode() {
        super(t("cali.board.startMeasureMode"), t("cali.board.startMeasureMode.tip"), "start-measure-mode", KeyStroke.getKeyStroke("pressed F5"));
    }

    @Override
    protected String getActionTitle() {
        return t("cali.board.startMeasureMode.title", ui.getModel().getBoardName());
    }

    @Override
    public void run() {
        CalibrationUIModel model = ui.getModel();
        model.stop();

        BoardUIModel board = model.getBoard();
        board.setCalibrationMode(false);
        Measure measure = ui.getContext().getBoardApi().startMeasureMode(board, board.getMeasure());
        board.startMeasureMode(measure);
    }
}
