package io.ultreia.ifremer.cali.ui.measure;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.ui.CaliActionSupport;
import org.nuiton.jaxx.runtime.awt.Clipboards;

import javax.swing.KeyStroke;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 14/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CopyMeasuresToClipboard extends CaliActionSupport<MeasureUI> {

    public CopyMeasuresToClipboard() {
        super(t("cali.measure.copyMeasuresToClipboard"), t("cali.measure.copyMeasuresToClipboard.tip"), "copy-measures-to-clipboard", KeyStroke.getKeyStroke("pressed F6"));
    }

    @Override
    protected String getActionTitle() {
        return t("cali.measure.copyMeasuresToClipboard.done", ui.getModel().getMeasuresCount());
    }

    @Override
    public void run() {
        String content = ui.getModel().getMeasuresToClipBoard();
        Clipboards.copyToClipBoard(content);
    }
}
