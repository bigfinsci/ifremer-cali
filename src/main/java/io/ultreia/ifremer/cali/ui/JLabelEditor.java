package io.ultreia.ifremer.cali.ui;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.bean.JavaBean;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializer;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializerSupport;

import javax.swing.JLabel;
import java.util.Objects;

/**
 * Created by tchemit on 10/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JLabelEditor extends JLabel {

    private final JavaBean bean;
    private String property;
    private String emptyText = "";

    public JLabelEditor(JavaBean bean) {
        this.bean = Objects.requireNonNull(bean);
    }

    public JavaBean getBean() {
        return bean;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getEmptyText() {
        return emptyText;
    }

    public void setEmptyText(String emptyText) {
        this.emptyText = emptyText;
    }

    public void init() {
        String propertyName = getProperty();
        if (propertyName == null) {
            propertyName = getName();
        }
        bean.addPropertyChangeListener(propertyName, evt -> {
            Object newValue = evt.getNewValue();
            setText(newValue == null ? getEmptyText() : newValue.toString());
        });
    }

    @AutoService(ComponentInitializer.class)
    public static class JLabelEditorInitializer extends ComponentInitializerSupport<JLabelEditor> {

        public JLabelEditorInitializer() {
            super(JLabelEditor.class);
        }

        @Override
        public void init(JAXXObject ui, JLabelEditor component) {
            component.init();
        }
    }

}
