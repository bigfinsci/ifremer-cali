package io.ultreia.ifremer.cali.ui.measure;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.ui.CaliActionSupport;
import org.nuiton.jaxx.runtime.application.UserMessages;

import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 14/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClearMeasures extends CaliActionSupport<MeasureUI> {

    private int measuresCount;

    public ClearMeasures() {
        super(t("cali.measure.clearMeasures"), t("cali.measure.clearMeasures.tip"), "clear-measures", KeyStroke.getKeyStroke("pressed F5"));
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        boolean canExecuteAction = super.canExecuteAction(e);
        if (canExecuteAction) {
            int response = UserMessages.askUser(t("cali.ask.user"), t("cali.measure.clearMeasures.confirm", ui.getModel().getMeasuresCount()), JOptionPane.QUESTION_MESSAGE, new String[]{
                    t("cali.option.delete"),
                    t("cali.option.cancel")
            }, 0);
            canExecuteAction = JOptionPane.OK_OPTION == response;
        }
        measuresCount = ui.getModel().getMeasuresCount();
        return canExecuteAction;
    }

    @Override
    protected String getActionTitle() {
        return t("cali.measure.clearMeasures.done", measuresCount);
    }

    @Override
    public void run() {
        ui.getModel().clearMeasures();
    }
}
