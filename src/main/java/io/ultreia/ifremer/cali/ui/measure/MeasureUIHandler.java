package io.ultreia.ifremer.cali.ui.measure;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.ifremer.cali.ui.CaliActionSupport;
import io.ultreia.ifremer.cali.ui.board.BoardUI;
import org.jdesktop.swingx.table.TableUtilities;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.spi.init.ComponentInitializerManager;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

/**
 * Created by tchemit on 14/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MeasureUIHandler implements UIHandler<MeasureUI> {

    private final MeasureUI ui;
    private JTable measures;

    MeasureUIHandler(MeasureUI ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(MeasureUI ui) {
        ComponentInitializerManager.get().apply(this.ui);
        MeasureUIModel model = this.ui.getModel();
        measures = ui.getMeasures();
        measures.setModel(model.tableModel());
        measures.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        measures.setFillsViewportHeight(true);
        measures.getModel().addTableModelListener(e -> {
            if (TableUtilities.isInsert(e)) {
                int firstRow = e.getFirstRow();
                SwingUtilities.invokeLater(() -> onDataAdded(firstRow));
            }
        });

        onMeasureCountChanged(0);
        model.addPropertyChangeListener("measuresCount", e -> onMeasureCountChanged((int) e.getNewValue()));
    }

    public void start(BoardUI parent) {
        ((CaliActionSupport) ui.getClearMeasures().getAction()).installAction(parent);
        ((CaliActionSupport) ui.getCopyMeasuresToClipboard().getAction()).installAction(parent);
        ((CaliActionSupport) ui.getStopMeasure().getAction()).installAction(parent);
    }

    private void onDataAdded(int firstRow) {
        measures.getSelectionModel().setSelectionInterval(firstRow, firstRow);
        SwingUtilities.invokeLater(() -> measures.scrollRectToVisible(measures.getCellRect(firstRow, 0, true)));
    }

    private void onMeasureCountChanged(int newValue) {
        boolean enabled = newValue > 0;
        ui.getClearMeasures().setEnabled(enabled);
        ui.getCopyMeasuresToClipboard().setEnabled(enabled);
    }
}
