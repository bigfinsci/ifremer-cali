package io.ultreia.ifremer.cali;

/*-
 * #%L
 * Ifremer :: CALI
 * %%
 * Copyright (C) 2018 Ultreia.io, Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.application.ApplicationBoot;
import org.nuiton.jaxx.runtime.application.ApplicationBootInitializerException;
import org.nuiton.jaxx.runtime.application.ApplicationBootInitializerSupport;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.Icon;
import java.awt.Color;

/**
 * Created by tchemit on 07/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class RunCali extends ApplicationBootInitializerSupport<CaliConfig, CaliContext> {

    public static final Color BG_COLOR = Color.WHITE;
    public static final Color INPUT_COLOR = Color.YELLOW;
    public static final Color KO_COLOR = Color.RED;
    public static final Color OK_COLOR = Color.GREEN.darker().darker();
    public static final Color WAITING_COLOR = Color.ORANGE.darker();
    public static final SingletonSupplier<Icon> ICON_ACCEPTED = SingletonSupplier.of(() -> SwingUtil.getUIManagerActionIcon("value-accepted"));
    public static final SingletonSupplier<Icon> ICON_WAITING = SingletonSupplier.of(() -> SwingUtil.getUIManagerActionIcon("value-waiting"));

    private Log log = LogFactory.getLog(RunCali.class);

    private RunCali(String... args) {
        super(args);
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            args = new String[]{"--default"};
        }
        ApplicationBoot.BOOT_LOG_PREFIX = "Application.boot [Cali]";
        ApplicationBoot.create(new RunCali(args)).startInThread();
    }

    @Override
    public CaliConfig createConfiguration(ApplicationBoot<CaliConfig, CaliContext> boot) throws ApplicationBootInitializerException {
        log.info(String.format("%s Create configuration (boot %s)", ApplicationBoot.BOOT_LOG_PREFIX, boot));
        return new CaliConfig();
    }

    @Override
    public CaliContext createContext(ApplicationBoot<CaliConfig, CaliContext> boot, CaliConfig configuration) {
        log.info(String.format("%s Create context (boot %s, configuration: %s)", ApplicationBoot.BOOT_LOG_PREFIX, boot, configuration));
        return new CaliContext(boot, configuration);
    }
}
