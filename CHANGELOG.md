# Ifremer CALI changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2018-04-06 11:50.

## Version [1.2](https://gitlab.com/ultreiaio/ifremer-cali/milestones/9)

**Closed at 2018-04-05.**

### Download
* [Application (cali-1.2.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/1.2/cali-1.2.zip)

### Issues
  * [[enhancement 20]](https://gitlab.com/ultreiaio/ifremer-cali/issues/20) **Add icon to application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 22]](https://gitlab.com/ultreiaio/ifremer-cali/issues/22) **Can&#39;t open device with firmware version 225** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1](https://gitlab.com/ultreiaio/ifremer-cali/milestones/8)

**Closed at 2018-02-28.**

### Download
* [Application (cali-1.1.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/1.1/cali-1.1.zip)

### Issues
  * [[enhancement 17]](https://gitlab.com/ultreiaio/ifremer-cali/issues/17) **Board type is never display** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 18]](https://gitlab.com/ultreiaio/ifremer-cali/issues/18) **Display temperature and humidity on selected board** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 19]](https://gitlab.com/ultreiaio/ifremer-cali/issues/19) **can&#39;t connect to board using firmware &gt; 2.25** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0](https://gitlab.com/ultreiaio/ifremer-cali/milestones/6)

**Closed at 2018-02-19.**

### Download
* [Application (cali-1.0.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/1.0/cali-1.0.zip)

### Issues
  * [[enhancement 13]](https://gitlab.com/ultreiaio/ifremer-cali/issues/13) **Display firmware version on connected board** (Thanks to Tony CHEMIT) (Reported by Vincent BADTS)
  * [[enhancement 14]](https://gitlab.com/ultreiaio/ifremer-cali/issues/14) **Add timeout + message when could not connect to a board** (Thanks to Tony CHEMIT) (Reported by Vincent BADTS)
  * [[enhancement 15]](https://gitlab.com/ultreiaio/ifremer-cali/issues/15) **Make Enter work on Windows environnement** (Thanks to Tony CHEMIT) (Reported by Vincent BADTS)

## Version [0.5](https://gitlab.com/ultreiaio/ifremer-cali/milestones/5)

**Closed at 2018-02-16.**

### Download
* [Application (cali-0.5.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/0.5/cali-0.5.zip)

### Issues
  * [[enhancement 7]](https://gitlab.com/ultreiaio/ifremer-cali/issues/7) **Add English language** (Thanks to Tony CHEMIT) (Reported by Vincent BADTS)
  * [[enhancement 8]](https://gitlab.com/ultreiaio/ifremer-cali/issues/8) **Make bluetooth works with Windows 64 architecture** (Thanks to Tony CHEMIT) (Reported by Vincent BADTS)
  * [[enhancement 9]](https://gitlab.com/ultreiaio/ifremer-cali/issues/9) **Remove Set data entry mode (just apply it at each device connection)** (Thanks to Tony CHEMIT) (Reported by Vincent BADTS)
  * [[enhancement 10]](https://gitlab.com/ultreiaio/ifremer-cali/issues/10) **Improve calibration flow** (Thanks to Tony CHEMIT) (Reported by Vincent BADTS)

## Version [0.4](https://gitlab.com/ultreiaio/ifremer-cali/milestones/4)

**Closed at 2018-02-15.**

### Download
* [Application (cali-0.4.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/0.4/cali-0.4.zip)

### Issues
  * [[enhancement 6]](https://gitlab.com/ultreiaio/ifremer-cali/issues/6) **Make calibration works** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.3](https://gitlab.com/ultreiaio/ifremer-cali/milestones/3)

**Closed at 2018-02-15.**

### Download
* [Application (cali-0.3.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/0.3/cali-0.3.zip)

### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/ifremer-cali/issues/4) **Get battery level works** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 5]](https://gitlab.com/ultreiaio/ifremer-cali/issues/5) **Perform only once SetDataEntryMode  on a connected device** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.2](https://gitlab.com/ultreiaio/ifremer-cali/milestones/2)
Implements communication to BigFin devices

**Closed at 2018-02-15.**

### Download
* [Application (cali-0.2.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/0.2/cali-0.2.zip)

### Issues
  * [[enhancement 2]](https://gitlab.com/ultreiaio/ifremer-cali/issues/2) **Implements Board Communication** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 3]](https://gitlab.com/ultreiaio/ifremer-cali/issues/3) **Implements measure mode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.1](https://gitlab.com/ultreiaio/ifremer-cali/milestones/1)

**Closed at 2018-02-05.**

### Download
* [Application (cali-0.1.zip)](http://repo1.maven.org/maven2/io/ultreia/ifremer/cali/0.1/cali-0.1.zip)

### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/ifremer-cali/issues/1) **Implements GUI** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

